/*jshint esversion: 6 */

const counter = function(arr){
    return `The number of elements are ${arr.length} in the array`;
};

const sumar = function (a,b){
    return `La suma de los valores es ${a+b}`;
};

var pi = 3.1416;

module.exports = {
    counter: counter,
    sumar: sumar,
    pi: pi
};