numero			[0-9]+ ("hola" | "adios")?
spc			[\t \u00a0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000]

%s EXPR

%%
"if"				return 'IF';
"else"				return 'ELSE';
"print"				return 'PRINT';
":"				return 'COLON';
"("				this.begin('EXPR'); return 'LPAREN';
")"				this.popState(); return 'RPAREN';
"+"				return 'PLUS';
"-"				return 'MINUS';
{numero}			{ console.log("entro"); return 'ID' ; }
\d+				return 'NATLITERAL';
<<EOF>>				return "ENDOFFILE";
[\n\r]+{spc}*/![^\n\r]		/* eat blank lines */
{spc}+				/* ignore all other whitespace */

%%