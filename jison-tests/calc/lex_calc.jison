/* lexical grammar */

Numero  [0-9]+ ("." [0-9]+ )?
Fecha {Numero} ":" {Numero} ":" {Numero}

spc			[\t \u00a0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000]


%%

\s+                   /* skip whitespace */
{Numero}              return 'Numero'
"*"                   return '*'
"/"                   return '/'
"-"                   return '-'
"+"                   return '+'
"^"                   return '^'
"!"                   return '!'
"%"                   return '%'
"("                   return '('
")"                   return ')'
';'                   return 'tk_ptcoma'
"PI"                  return 'PI'
"E"                   return 'E'
<<EOF>>               return 'EOF'
.                     return 'INVALID'

[\n\r]+{spc}*/![^\n\r]		/* eat blank lines */
{spc}+				/* ignore all other whitespace */

%%