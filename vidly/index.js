/* jshint esversion:6 */

const express = require('express');
const app = express();
const Joi = require('joi');
app.use(express.json());

const genres = [
    {id:1, name:'comedy'},
    {id:2, name:'thriller'},
    {id:3, name:'terror'}
];

app.get('/api/genres', (req,res) => {
    res.send(genres);
});

app.get('/api/genres/:id', (req,res) => {
    let genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send(`The requested ID[${req.params.id}] was not found.`);
    res.send(genre);
});

app.post('/api/genres', (req,res) => {
    const { error } = ValidateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let genre = {
        id: genres.length + 1,
        name: req.body.name
    };

    genres.push(genre);
    res.send(genre);
});

app.put('/api/genres/:id', (req,res) => {
    let genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send(`The requested ID[${req.params.id}] was not found.`);

    const { error } = ValidateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    genre.name = req.body.name;
    res.send(genre);
});

app.delete('/api/genres/:id', (req,res) => {
    let genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send(`The requested ID[${req.params.id}] was not found.`);

    const index = genres.indexOf(genre);
    genres.splice(index,1);
    res.send(genre);
});


function ValidateGenre(genre){
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(genre,schema);
}

const port = process.env.PORT || 3000 ;
app.listen(port, () => console.log(`Listening to port ${port}...`));